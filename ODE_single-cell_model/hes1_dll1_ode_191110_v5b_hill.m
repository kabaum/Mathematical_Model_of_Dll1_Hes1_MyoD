function[dx,J]=hes1_dll1_ode_191110_v5b_hill(t,x,pars,nlpars) 
% ODE model of Hes1, Dll1, MyoD mRNA and protein dynamics 
% Established in Zhang et al. 2021
%
%variables
%%%%%%%%%%
%x is a vector of length 8 encoding
%x(1): Hes1 protein, HP
%x(2): Hes1 mRNA, HR
%x(3): Hes1 interacting factor, HF
%x(4): MyoD mRNA, MR
%x(5): MyoD protein, MP
%x(6): MyoD interacting factor, MF
%x(7): Dll1 mRNA, DR
%x(8): Dll1 protein, DP
%Attention: the order of variables is different from the ordering in the
%model description in Zhang et al.
%
%kinetic parameters
%%%%%%%%%%%%%%%%%%%
%Parameters pars(1) to pars(7) carry values as from model of 
%Kageyama et al. 2002 Science
%pars(1)=k_2: combined degradation Hes1 and interacting factor 0.022
%pars(2)=k_1: Hes1 protein translation 0.3
%pars(3)=k_3: Hes1 protein degradation rate constant 0.031
%pars(4)=k_5: Hes1 mRNA degradataion rate constant 0.028
%pars(5)=k_4: Hes1 mRNA transcription rate constant 0.5
%pars(6)=k_6: Hes1 interaction factor translation 20
%pars(7)=k_7: Hes1 interaction factor degradation 0.3
%
%Parameters pars(8)-pars(14) carry values as from model of 
%Lahmann et al. 2019
%pars(8)=k_11: myoD mRNA transcription rate constant
%pars(9)=k_12: myoD mRNA degradation rate constant
%pars(10)=k_8: myoD protein translation rate constant
%pars(11)=k_10: myoD protein degradation rate constant
%pars(12)=k_13: myoD interacting factor translation rate constant
%pars(13)=k_14: myoD interacting factor degradation rate constant
%pars(14)=k_9: combined myoD interacting factor degradation rate constant
%
%Dll1-related parameters
%pars(15)=k_17: Dll1 mRNA transcription rate constant
%pars(16)=k_18: Dll1 mRNA degradation rate constant
%pars(17)=k_15: Dll1 protein translation rate
%pars(18)=k_16: Dll1 protein degradation rate constant
%
%further parameters that are related to regulation
%nlpars(1)=1: not changeable in the model, Hes protein inhibition on Hes mRNA
%nlpars(2)=1: not changeable in the model, Hes protein inhibition on Hes
%interacting factor
%nlpars(3)=1: not changeable in the model, Hes protein inhibition on MyoD
%mRNA
%nlpars(4)=1: not changeable in the model, Hes protein inhibition on MyoD
%interacting factor
%nlpars(5)=1: not changeable in the model, Hes protein inhibition on Dll1
%mRNA
%nlpars(6)=k_20: MyoD activation coefficient 
%nlpars(7)=2: not changeable in the model, Hill coefficient (exponent) of MyoD in Dll1 activation
%nlpars(8)=2: not changeable in the model, Hes protein inhibition Hill
%coefficient (exponent) on Dll1 mRNA
%nlpars(9)=k_19: MyoD-independent Dll1 mRNA expression

dx=zeros(8,1);
J=zeros(8,8);
dx(1)=pars(2)*x(2)-pars(1)*x(1)*x(3)-pars(3)*x(1); %Hes1 protein
dx(2)=pars(5)/(nlpars(1)+x(1)^2)-pars(4)*x(2); %Hes1 mRNA
dx(3)=pars(6)/(nlpars(2)+x(1)^2)-pars(1)*x(1)*x(3)-pars(7)*x(3); %Hes1 interacting factor
dx(4)=pars(8)/(nlpars(3)+x(1)^2)-pars(9)*x(4); %MyoD mRNA
dx(5)=pars(10)*x(4)-pars(14)*x(5)*x(6)-pars(11)*x(5); %myod protein
dx(6)=pars(12)/(nlpars(4)+x(1)^2)-pars(13)*x(6)-pars(14)*x(5)*x(6);%myod interacting facot
dx(7)=pars(15)*(nlpars(9)+x(5)^nlpars(7)/nlpars(6))*1/(nlpars(5)+x(1)^nlpars(8))-pars(16)*x(7); %Dll1 mRNA
dx(8)=pars(17)*x(7)-pars(18)*x(8); %Dll1 protein
end