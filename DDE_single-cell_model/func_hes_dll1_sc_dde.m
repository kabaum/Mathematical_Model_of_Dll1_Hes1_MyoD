function[dx] = func_hes_dll1_sc_dde(t,x,z,pars)
% DDE model of Hes and Dll1 protein abundance in a single cell
%
%variables
%%%%%%%%%%
%x is a vector of length 2 encoding
%x(1): Hes1 
%x(2): Dll1 
%
%kinetic parameters 
%%%%%%%%%%%%%%%%%%%
%Please note that pars(i) corresponds to k_i in the model description.
%pars(1): Hes1 protein synthesis, v in Shimojo 2016: 10
%pars(2): Hes1 regulating itself, K1 in Shimojo 2016: 1
%pars(3): Hes1 degradation, r in Shimojo 2016: 2
%pars(4): Dll1 protein production (inhibited by Hes protein plus time
%lag 2)
%pars(5): inhibition constant of Hes1 inhibition of Dll1 protein
%production
%pars(6): Dll1 protein degradation
%
%delays
%%%%%%%
%z(1,1): Hes1 with time lag 1 (tau_1)
%z(1,2): Hes1 with time lag 2 (tau_21)
% time lag 1 = tau_1: Hes regulation on itself 
% time lag 2 = tau_21: Hes regulation on Dll1
%Explicit values of the delays are entered at model integration only.
%
dx=zeros(2,1);
dx(1)=pars(1)*pars(2)^2/(pars(2)^2 + z(1,1)^2)-pars(3)*x(1); %Hes1 (protein)
dx(2)=pars(4)*pars(5)^2/(pars(5)^2 + z(1,2)^2)-pars(6)*x(2); %Dll1 protein

end