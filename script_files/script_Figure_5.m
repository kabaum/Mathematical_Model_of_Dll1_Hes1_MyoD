%% Figure 5 b
% Dynamics for ODE model of a single cell: Hes1, MyoD, Dll1 protein
% abundance

%setting kinetic parameters as described in the model equations
opt_fin_pars=[...%these first 7 are given by Hirata model
    0.022,...modulation of Hes1 protein synthesis by Hes1-interacting factor (irrev. complex formation)
    0.3,...Hes1 protein synthesis
    0.031,... Hes1 protein degradation
    0.028,... Hes1 mRNA degradation
    0.5,...synthesis of Hes1 mRNA (Kageyama: inhibition rate of Hes1 mRNA by Hes1 protein) 
    20,...synthesis of Hes1 interacting factor (Kag: inhibition rate of Hes1-interacting factor by Hes1 protein)
    0.3,... degradation rate of Hes1 interacting factor %these first 7 are given by Hirata model
    0.05,...%myod mrna transcription
    0.0077,...%myod mrna degradation, given by half-lives
    0.6,...%myod protein translation rate constant
    0.014,...]; %myod protein degradation, given by half-lives
    4,... %transcription MYOD interacting protein
    0.03,... %degradation MyoD interacting protein
    0.02,...%interacting MyoD/MyoD irrev. complex formation %%% here are the new parameters from the model with Dll1
    0.02,...%Dll1 mrna transcription
    0.0231,...%Dll1 mrna degradation --> assume 30 minutes
    0.1,...%Dll1 protein translation rate constant, unknown, chosen similar to MyoD
    0.017]; %Dll1 protein degradation rate, derived from half-live being 40 minutes (Imayoshi et al, 2013)

opt_fin_nlpars=[1,... %Hes1 mRNA inhibition by Hes protein
    1,... %Hes1 interacting protein inhibition by Hes protein
    1,... %Myod mRNA inhibition by Hes protein
    1,... %Myod interacting protein
    1,... %Dll1 mRNA inhibition by Hes1 protein
    5,....]; %Dll1 mRNA activation by MyoD protein, kn6
    2,... %Hill coefficient MyoD activation of Dll1 mRNA
    2,... %Hill coefficient Hes1 inhibition of Dll1 mRNA
    10]; %kn9: constitutive Dll1 mRNA expression

%initialize WT model
[t,y]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars,opt_fin_nlpars),[0,9875:1:10000],[1 1 1 1 1 1 1 1]);
y0 = [y(2:end,1:8)];
t0= [t(2:end)-t(2)];

%integrate WT yet a bit longer
[t,y]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars,opt_fin_nlpars),[0,650],y0(end,:));

%plotting WT dynamics of Hes1, Dll1, MyoD
plot(t,y(:,1),'LineWidth',2,'Color','#0072BD');
hold on
plot(t,y(:,5),'LineWidth',2,'Color','#77AC30');
%plot(t(cutoff:end)-t(cutoff),y(cutoff:end,7),'LineWidth',2,'Color','#D95319');
plot(t,y(:,8),'LineWidth',2.5,'Color','#A2142F');
lgd=legend('Hes1 protein', 'MyoD protein', 'Dll1 protein');
lgd.Location='northeastoutside';
xlim([0 600])
ylim([0 10])
xlabel('time [min]')
ylabel('concentration [a.u.]')
title('Wildtype')
hold off
h = gcf;
pos = get(h,'Position'); 
%pos =    1     1   457   230
set(h,'PaperPositionMode','auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
print(h,'200127_final_model_WT.pdf','-dpdf')
%Figure 5 b






%% Figure 5 d
% Dynamics for DDE model coupling two cells: coupling two identical cells
% WT (left), two mutant cells (middle) and 
% coupling one mutant and one WT cell (right)

pars=[100,...v for Hes1
    1,... K1 for Hes1
    2.6,... r for Hes1 (degradation)
    11.2,... Dll1 synthesis
    5.5,...K1 for Dll1 - same as for Hes1
    1.04 ]; ... Dll1 degradation in /h: (half-life: 40 min=2/3h)--> 3/2*log(2)=1.0397
%combine into the same parameters for both coupled cells
pars2=[pars,pars];

%DDE 2 coupled WT cells
[sol_2diff_coupled_cells_wt] = dde23(@(t,x,z)func_hes_dll1_2c3_noise_dde(t,x,z,pars2),[0.725,0.35,1.3,0.725,0.35,1.3],...
                rand(4,1)*10,[0,1000],...
            ddeset('AbsTol',1e-4,'RelTol', 1e-5));
sol_2diff_coupled_cells_wt = deval(sol_2diff_coupled_cells_wt,990:0.01:1000);

%DDE 2 mut cells (distinguished from WT by different delay only)
[sol_2diff_coupled_cells_mut] = dde23(@(t,x,z)func_hes_dll1_2c3_noise_dde(t,x,z,pars2),[0.725,0.45,1.3,0.725,0.45,1.3],...
                rand(4,1)*10,[0,1000],...
            ddeset('AbsTol',1e-4,'RelTol', 1e-5));
sol_2diff_coupled_cells_mut = deval(sol_2diff_coupled_cells_mut,990:0.01:1000);

%DDE coupling one mutant, 1 WT cell
[sol_wt_mut_coupled_cells_k750] = dde23(@(t,x,z)func_hes_dll1_2c3_noise_dde(t,x,z,pars2),[0.725,0.35,1.3,0.725,0.45,1.3],...
                rand(4,1)*10,[0,1000],...
            ddeset('AbsTol',1e-4,'RelTol', 1e-5));
sol_wt_mut_coupled_cells_k750 = deval(sol_wt_mut_coupled_cells_k750,990:0.01:1000);

%plotting parameters
ylmin_dll=9.6;
ylmax_dll=9.85;
ylmax_hes=2.15;
ylmin_hes=1.45;
tiledlayout(3,2)
nexttile
    hold on
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_wt(1,:),'linewidth',2,'color','#003380')
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_wt(3,:),'linewidth',2,'color','#5599FF')
    xlabel({'time [min]'})
    ylabel({'Hes abundance [a.u.]'})
    title({'WT original coupled cell oscillations'})
    legend({'Hes cell 1', 'Hes cell 2'})
    ylim([ylmin_hes,ylmax_hes])
    box on
nexttile
    hold on
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_wt(2,:),'linewidth',2,'color','#782121')
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_wt(4,:),'linewidth',2,'color','#FF7F2A')
    xlabel({'time [min]'})
    ylabel({'Dll1 abundance [a.u.]'})
    title({'WT original coupled cell oscillations'})
    legend({'Dll1 cell 1', 'Dll1 cell 2'})
    ylim([ylmin_dll,ylmax_dll])
    box on
nexttile
    hold on
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_mut(1,:),'linewidth',2,'color','#003380')
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_mut(3,:),'linewidth',2,'color','#5599FF')
    xlabel({'time [min]'})
    ylabel({'Hes abundance [a.u.]'})
    title({'Mut type 2 coupled cell oscillations'})
    ylim([ylmin_hes,ylmax_hes])
    box on
nexttile
    hold on
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_mut(2,:),'linewidth',2,'color','#782121')
    plot(0:0.01*60:10*60,sol_2diff_coupled_cells_mut(4,:),'linewidth',2,'color','#FF7F2A')
    xlabel({'time [min]'})
    ylabel({'Dll1 abundance [a.u.]'})
    title({'Mut type 2 coupled cell oscillations'})
    ylim([ylmin_dll,ylmax_dll])
    box on
nexttile
    hold on
    plot(0:0.01*60:10*60,sol_wt_mut_coupled_cells_k750(1,:),'linewidth',2,'color','#003380')
    plot(0:0.01*60:10*60,sol_wt_mut_coupled_cells_k750(3,:),'linewidth',2,'color','#5599FF')
    xlabel({'time [min]'})
    ylabel({'Hes abundance [a.u.]'})
    title({'Coupling WT cell (cell 1) and', 'mut type 2 cell (cell 2)'})
    ylim([ylmin_hes,ylmax_hes])
    box on
nexttile
    hold on
    plot(0:0.01*60:10*60,sol_wt_mut_coupled_cells_k750(2,:),'linewidth',2,'color','#782121')
    plot(0:0.01*60:10*60,sol_wt_mut_coupled_cells_k750(4,:),'linewidth',2,'color','#FF7F2A')
    xlabel({'time [min]'})
    ylabel({'Dll1 abundance [a.u.]'})
    title({'Coupling WT cell (cell 1) and', 'mut type 2 cell (cell 2)'})
    ylim([ylmin_dll,ylmax_dll])
    box on
h=gcf
%h.Position=[304 269 660 536]
pos=h.Position %78          63        1268         735
set(h,'PaperPositionMode','auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
print(h,'201118_DDE_2c_wt_mut.pdf','-dpdf')
%Figure 5d - here for Dll1 and Hes, in the manuscript only shown for Dll1
