
%% Supplementary Figure 6

%% Supplementray Figure 6a
% Dll1 dynamics under MyoD KO in the single-cell ODE model

% kinetic parameters 
opt_fin_pars=[...%these first 7 are given by Hirata model
    0.022,...modulation of Hes1 protein synthesis by Hes1-interacting factor (irrev. complex formation)
    0.3,...Hes1 protein synthesis
    0.031,... Hes1 protein degradation
    0.028,... Hes1 mRNA degradation
    0.5,...synthesis of Hes1 mRNA (Kageyama: inhibition rate of Hes1 mRNA by Hes1 protein) 
    20,...synthesis of Hes1 interacting factor (Kag: inhibition rate of Hes1-interacting factor by Hes1 protein)
    0.3,... degradation rate of Hes1 interacting factor %these first 7 are given by Hirata model
    0.05,...%myod mrna transcription
    0.0077,...%myod mrna degradation, given by half-lives
    0.6,...%myod protein translation rate constant
    0.014,...]; %myod protein degradation, given by half-lives
    4,... %transcription MYOD interacting protein
    0.03,... %degradation MyoD interacting protein
    0.02,...%interacting MyoD/MyoD irrev. complex formation %%% here are the new parameters from the model with Dll1
    0.02,...%Dll1 mrna transcription
    0.0231,...%Dll1 mrna degradation --> assume 30 minutes
    0.1,...%Dll1 protein translation rate constant, unknown, chosen similar to MyoD
    0.017]; %Dll1 protein degradation rate, derived from half-live being 40 minutes (Imayoshi et al, 2013)

opt_fin_nlpars=[1,... %Hes1 mRNA inhibition by Hes protein
    1,... %Hes1 interacting protein inhibition by Hes protein
    1,... %Myod mRNA inhibition by Hes protein
    1,... %Myod interacting protein
    1,... %Dll1 mRNA inhibition by Hes1 protein
    5,....]; %Dll1 mRNA activation by MyoD protein, kn6
    2,... %Hill coefficient MyoD activation of Dll1 mRNA
    2,... %Hill coefficient Hes1 inhibition of Dll1 mRNA
    10]; %kn9: constitutive Dll1 mRNA expression


% initial WT integration
[t,y]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars,opt_fin_nlpars),[0,9875:1:10000],[1 1 1 1 1 1 1 1]);
y0 = [y(2:end,1:8)];
t0= [t(2:end)-t(2)];

% compute WT ODE model for longer
[t,y]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars,opt_fin_nlpars),[0,650],y0(end,:));

% MyoD KO initial computation
opt_fin_pars_myodKO= opt_fin_pars;
opt_fin_pars_myodKO(8)=0;
[t_myodko,y_myodko]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars_myodKO,opt_fin_nlpars),[0,9875:1:10000],[1 1 1 1 1 1 1 1]);
y0_myodko = [y_myodko(2:end,1:8)];
t0_myodko=t_myodko(2:end)-t_myodko(2);

% compute MyoDKO cell ODE model for longer for plotting
[t_myodko,y_myodko]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars_myodKO,opt_fin_nlpars),[0,650],y0_myodko(end,:));

plot(t,y(:,8),'-','LineWidth',2,'Color','#A2142F');
hold on
plot(t_myodko,y_myodko(:,8),'--','LineWidth',2,'Color','#A2142F');
hold off
lgd=legend('WT', 'MyoDKO');
lgd.Location='northeastoutside';
xlim([0 600])
ylim([0 8.5])
xlabel('time [min]')
ylabel('Dll1 protein concentration [a.u.]')
title('Dll1 protein - WT vs. MyoDKO')
hold off
h = gcf;
h.Position =    [ 1     1   457   230];
%pos=  1     1   457   230
set(h,'PaperPositionMode','auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
print(h,'200127_final_model_Dll1_prot_WT_MyoDKO.pdf','-dpdf')
% Supplementary Figure 6a



%% Supplementary Figure 6b
% Dll1 dynamics under Hes KO in the single-cell ODE model

opt_fin_pars=[...%these first 7 are given by Hirata model
    0.022,...modulation of Hes1 protein synthesis by Hes1-interacting factor (irrev. complex formation)
    0.3,...Hes1 protein synthesis
    0.031,... Hes1 protein degradation
    0.028,... Hes1 mRNA degradation
    0.5,...synthesis of Hes1 mRNA (Kageyama: inhibition rate of Hes1 mRNA by Hes1 protein) 
    20,...synthesis of Hes1 interacting factor (Kag: inhibition rate of Hes1-interacting factor by Hes1 protein)
    0.3,... degradation rate of Hes1 interacting factor %these first 7 are given by Hirata model
    0.05,...%myod mrna transcription
    0.0077,...%myod mrna degradation, given by half-lives
    0.6,...%myod protein translation rate constant
    0.014,...]; %myod protein degradation, given by half-lives
    4,... %transcription MYOD interacting protein
    0.03,... %degradation MyoD interacting protein
    0.02,...%interacting MyoD/MyoD irrev. complex formation %%% here are the new parameters from the model with Dll1
    0.02,...%Dll1 mrna transcription
    0.0231,...%Dll1 mrna degradation --> assume 30 minutes
    0.1,...%Dll1 protein translation rate constant, unknown, chosen similar to MyoD
    0.017]; %Dll1 protein degradation rate, derived from half-live being 40 minutes (Imayoshi et al, 2013)

opt_fin_nlpars=[1,... %Hes1 mRNA inhibition by Hes protein
    1,... %Hes1 interacting protein inhibition by Hes protein
    1,... %Myod mRNA inhibition by Hes protein
    1,... %Myod interacting protein
    1,... %Dll1 mRNA inhibition by Hes1 protein
    5,....]; %Dll1 mRNA activation by MyoD protein, kn6
    2,... %Hill coefficient MyoD activation of Dll1 mRNA
    2,... %Hill coefficient Hes1 inhibition of Dll1 mRNA
    10]; %kn9: constitutive Dll1 mRNA expression


%intgrate for WT 
[t,y]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars,opt_fin_nlpars),[0,9875:1:10000],[1 1 1 1 1 1 1 1]);
y0 = [y(2:end,1:8)];
t0= [t(2:end)-t(2)];

%change parameters to HesKO
opt_fin_pars_hesko= opt_fin_pars;
opt_fin_pars_hesko(5)=0;
[t_hesko,y_hesko]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars_hesko,opt_fin_nlpars),[0,9875:1:10000],[1 1 1 1 1 1 1 1]);
y0_hesko = [y_hesko(2:end,1:8)];
t0_hesko=t_hesko(2:end)-t_hesko(2);

%integrate longer for plotting
%HESKO
[t0_hesko,y0_hesko]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars_hesko,opt_fin_nlpars),[0,59875:1:60500],y0_hesko(end,:));
t0_hesko=t0_hesko(2:end);
y0_hesko=y0_hesko(2:end,:);
%WT
[t0,y0]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars,opt_fin_nlpars),[0,59875:1:60500],y0(end,:));
t0=t0(2:end);
y0=y0(2:end,:);

%plot Dll1 dynamics
plot(t0_hesko-t0_hesko(1),y0(:,8),'LineWidth',2,'Color','#A2142F');
box on
hold on
plot(t0_hesko-t0_hesko(1),y0_hesko(:,8)*1/35,'--','LineWidth',2,'Color','#A2142F');
lgd=legend('WT', 'HesKO (scaled 1/35)');
title('Dll1 protein');
lgd.Location='northeastoutside';
xlabel('time [min]')
ylabel('concentration [a.u.]')
xlim([0 600])
h = gcf;
h.Position =    [ 1     1   422   200];
pos = get(h,'Position'); 
set(h,'PaperPositionMode','auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
print(h,'201214_HesKO_Hes_MyoD_Dll1_timecourse_abs_alt.pdf','-dpdf')
% Supplementary Figure 6b
% y-axis has been cut as indicated in the figure







%% Supplementary Figure 6e
%Mapping DDE single-cell model to ODE singel-cell model dynamics

%parameters DDE
pars=[100,...v for Hes1
    1,... K1 for Hes1
    2.6,... r for Hes1 (degradation)
    11.2,... Dll1 synthesis
    5.5,...K1 for Dll1 - same as for Hes1
    1.04 ]; ... Dll1 degradation in /h: (half-life: 40 min=2/3h)--> 3/2*log(2)=1.0397
%delay for Dll1: 0.725
dll1_del = 0.35;
[sol_dde1] = dde23(@(t,x,z)func_hes_dll1_sc_dde(t,x,z,pars),[0.725,dll1_del],@historyfunc_hes_dll1_sc_dde,[0,200]);
temp_sol1= deval(sol_dde1,65.7823:0.01:115.7823);

%ODE model parameters
opt_fin_pars=[...%these first 7 are given by Hirata model
    0.022,...modulation of Hes1 protein synthesis by Hes1-interacting factor (irrev. complex formation)
    0.3,...Hes1 protein synthesis
    0.031,... Hes1 protein degradation
    0.028,... Hes1 mRNA degradation
    0.5,...synthesis of Hes1 mRNA (Kageyama: inhibition rate of Hes1 mRNA by Hes1 protein) 
    20,...synthesis of Hes1 interacting factor (Kag: inhibition rate of Hes1-interacting factor by Hes1 protein)
    0.3,... degradation rate of Hes1 interacting factor %these first 7 are given by Hirata model
    0.05,...%myod mrna transcription
    0.0077,...%myod mrna degradation, given by half-lives
    0.6,...%myod protein translation rate constant
    0.014,...]; %myod protein degradation, given by half-lives
    4,... %transcription MYOD interacting protein
    0.03,... %degradation MyoD interacting protein
    0.02,...%interacting MyoD/MyoD irrev. complex formation %%% here are the new parameters from the model with Dll1
    0.02,...%Dll1 mrna transcription
    0.0231,...%Dll1 mrna degradation --> assume 30 minutes
    0.1,...%Dll1 protein translation rate constant, unknown, chosen similar to MyoD
    0.017]; %Dll1 protein degradation rate, derived from half-live being 40 minutes (Imayoshi et al, 2013)

opt_fin_nlpars=[1,... %Hes1 mRNA inhibition by Hes protein
    1,... %Hes1 interacting protein inhibition by Hes protein
    1,... %Myod mRNA inhibition by Hes protein
    1,... %Myod interacting protein
    1,... %Dll1 mRNA inhibition by Hes1 protein
    5,....]; %Dll1 mRNA activation by MyoD protein, kn6
    2,... %Hill coefficient MyoD activation of Dll1 mRNA
    2,... %Hill coefficient Hes1 inhibition of Dll1 mRNA
    10]; %kn9: constitutive Dll1 mRNA expression

%ODE solution WT
[solode_t,solode_y]=ode45(@(t,x)hes1_dll1_ode_191110_v5b_hill(t,x,opt_fin_pars,opt_fin_nlpars),[0,60000:1:72000],[1 1 1 1 1 1 1 1]);

%plot DDE and ODE both together, Hes and Dll1 protein
hold off
plot((solode_t(2:end)-solode_t(2)),solode_y(2:end,[1,8]),'linewidth', 2,'color',[0.8,0.8,0.8])
hold on
plot(0:0.01*60:50*60,temp_sol1(1,:),'linewidth',2,'color','#0000FF')
plot(0:0.01*60:50*60,temp_sol1(2,:),'linewidth',2,'color','#FF0000')
xlim([0,10*60])
ylim([0,10])
legend({'ODE: Hes1',...
    'ODE: Dll1',...
    'DDE: Hes1',...
    'DDE: Dll1'},'location','northeastoutside')
xlabel('time [min]')
ylabel('protein abundance [a.u.]')
h=gcf
h.WindowStyle= 'normal';
h.Position=[1 1 560 200];
title('Mapping DDE to ODE model dynamics')
pos=h.Position;
set(h,'PaperPositionMode','auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
print(h,'200827_ODE_DDE_sc_1stgoodfit.pdf','-dpdf')
%Supplementary Figure 6e


%% Supplementary Figure 6f
%Dll1-type 2 mutation in the DDE single-cell model

%Wildtype model
pars=[100,...v for Hes1
    1,... K1 for Hes1
    2.6,... r for Hes1 (degradation)
    11.2,... Dll1 synthesis
    5.5,...K1 for Dll1 - same as for Hes1
    1.04 ]; ... Dll1 degradation in /h: (half-life: 40 min=2/3h)--> 3/2*log(2)=1.0397
%delay for Dll1: 0.725
dll1_del = 0.35;
[sol_dde1] = dde23(@(t,x,z)func_hes_dll1_sc_dde(t,x,z,pars),[0.725,dll1_del],@historyfunc_hes_dll1_sc_dde,[0,200]);
temp_sol1= deval(sol_dde1,150:0.01:200);

%mutant model
dll1_del = 0.35+0.1;
[sol_dde1mut] = dde23(@(t,x,z)func_hes_dll1_sc_dde(t,x,z,pars),[0.725,dll1_del],@historyfunc_hes_dll1_sc_dde,[0,200]);
temp_sol1_mut2= deval(sol_dde1mut,150:0.01:200);



hold off
plot(0:0.01*60:50*60,temp_sol1(1,:),'linewidth',2,'color','#0000FF')
hold on
plot(0:0.01*60:50*60,temp_sol1(2,:),'linewidth',2,'color','#FF0000')
plot(0:0.01*60:50*60,temp_sol1_mut2(1,:),'-.','linewidth',2,'color','#000090')
plot(0:0.01*60:50*60,temp_sol1_mut2(2,:),'-.','linewidth',2,'color','#900000')
xlim([0,6*60])
xlabel('time [min]')
ylabel('protein abundance [a.u.]')
title({'WT and mutant that has \tau_{21} increased by 6 min'})
legend({'Hes1 WT', 'Dll1 WT', 'Hes1 mut','Dll1 mut'},'location','northeastoutside')
ylim([0,10])
h=gcf
h.WindowStyle= 'normal'
h.Position=[1 1 560 200]
pos=h.Position
set(h,'PaperPositionMode','auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
print(h,'200827_ODE_DDE_sc_WT_type2mut.pdf','-dpdf')
%Supplementary Figure 6f

% compute phase shift reported in the Figure legend
%using function phase_shift_calculation_gen
pars=[100,...v for Hes1
    1,... K1 for Hes1
    2.6,... r for Hes1 (degradation)
    11.2,... Dll1 synthesis
    5.5,...K1 for Dll1 - same as for Hes1
    1.04 ]; ... Dll1 degradation in /h: (half-life: 40 min=2/3h)--> 3/2*log(2)=1.0397
%delay for Dll1: 0.725
dll1_del = 0.35;
[sol_dde1] = dde23(@(t,x,z)func_hes_dll1_sc_dde(t,x,z,pars),[0.725,dll1_del],@historyfunc_hes_dll1_sc_dde,[0,300]);
fac=100;
period=2.1;
timetol=0.0001;
tsol=[0,fac*period,(fac+1)*period:(timetol*period):(fac+4)*period];
ysol_wt= transpose(deval(sol_dde1,tsol));

%mutant model
dll1_del = 0.35+0.1;
[sol_dde1mut] = dde23(@(t,x,z)func_hes_dll1_sc_dde(t,x,z,pars),[0.725,dll1_del],@historyfunc_hes_dll1_sc_dde,[0,300]);
ysol_mut= transpose(deval(sol_dde1mut,tsol));

[phase_shift_WT,exact_period_WT]=phase_shift_calculation_gen(1,2,tsol,ysol_wt)
%phase shift: 1.9051
[phase_shift_mut,exact_period_mut]=phase_shift_calculation_gen(1,2,tsol,ysol_mut)
%phase shift:  2.0042

%% Supplementary Figure 6g
% Bifurcation for tau_21 for the DDE two-cell model coupling identical
% cells

pars=[100,...v for Hes1
    1,... K1 for Hes1
    2.6,... r for Hes1 (degradation)
    11.2,... Dll1 synthesis
    5.5,...K1 for Dll1 - same as for Hes1
    1.04 ]; ... Dll1 degradation in /h: (half-life: 40 min=2/3h)--> 3/2*log(2)=1.0397
%delay for Dll1: 0.725
%tau_22_0 = 78/60=1.3;
pars(7)=50; %k7=50
delay_21 = [0.1:0.02:1.2];

synch_tab_21=zeros(1,length(delay_21));
hes_amp_21 = zeros(1,length(delay_21));
dll1_amp_21 = zeros(1,length(delay_21));

for i = 1:length(delay_21)
        i
        %WT cell
        [sol_dde3] = dde23(@(t,x,z)func_hes_dll1_2c3_dde(t,x,z,pars),[0.725,delay_21(i),1.3],...
                rand(4,1)*10,[0,1000],...
            ddeset('AbsTol',1e-4,'RelTol', 1e-5));
        end_sol = deval(sol_dde3,990:0.01:1000);
        hes_amp_21(1,i) = max(end_sol(1,:))-min(end_sol(1,:));
        dll1_amp_21(1,i) = max(end_sol(2,:))-min(end_sol(2,:));
        synch_tab_21(1,i) = max(abs(end_sol(3,:)- end_sol(1,:)))<0.001;
        
end
 
save('201118_osc_death_bif_tau21','delay_21','synch_tab_21','hes_amp_21','dll1_amp_21')

%plot bifurcation along tau_21

tiledlayout(1,1)
nexttile
%this is k7=50
    xline(0.35,'--','Color','#080808','HandleVisibility','off','Linewidth',1.5)
    hold on
    xline(0.45,'--','Color','#080808','HandleVisibility','off','Linewidth',1.5)
    plot([delay_21],...
        cat(1,hes_amp_21,...
            dll1_amp_21),'Linewidth',2)
    hold on
    xlabel('tau_{21} [h]')
    ylabel('amplitude (maximal-minimal value)')
    xlim([0.1,1])
    ylim([0,1.1])
    title(['bifurcation along \tau_{21}']) 
    box on
    legend({'Hes1 amplitude', 'Dll1 amplitude'},'Location','northeastoutside')

h=gcf
%h.Position =[437 554 533 244]
pos=h.Position;
set(h,'PaperPositionMode','auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
    print(h,['201118_DDE_2Cm3_Hes_Dll1_mut_tau21bif_k7_50_tau22_1pt3.pdf'],'-dpdf')
%Supplementary Figure 6g
    
