function[phase_shift,exact_period]=phase_shift_calculation_gen(refspec,calcspec,tsol,ysol)
%function to compute the phase shift from species refspec to the species
%calcspec
%for the solution of a function with times tsol, concentrations ysol,
%expected period, and tolerance expected
%
%fac=50
%timetol=0.0001
%period=3*60
%[tsol,ysol]=ode45(model_func,...
%    [0,fac*period,(fac+1)*period:(timetol*period):(fac+4)*period],y0);
%or 
%tsol=[0,fac*period,(fac+1)*period:(timetol*period):(fac+4)*period];
%ysol=transpose(deval(sol_dde1,tsol))
%
%phase_shift_calculation(1,[7,8],tsol,ysol)

%integrate a long time to achieve limit cycle for all species, with
%tolerance as supplied in the input parameters


%go to the middle of the integration interval and search for local maxima
%to the left and to the right
t0_ind=round(length(tsol)/2);
%find local maxima for the reference species to the left of t0_ind
ttemp=t0_ind;
maxvaltemp = ysol(t0_ind,refspec); %set the maximal value to the value at t0
while not(maxvaltemp>ysol(ttemp+1,refspec) && ...
    maxvaltemp>ysol(ttemp-1,refspec))
    ttemp=ttemp-1;
    maxvaltemp=ysol(ttemp,refspec);
end
tmaxind_left=ttemp;
%if t0_ind happens to be a local maximum, we start 1 index further to the
%right
if tmaxind_left==t0_ind
    t0_ind_right=t0_ind+1;
else
    t0_ind_right=t0_ind;
end

%find local maximum to the right
ttemp=t0_ind_right;
maxvaltemp = ysol(t0_ind_right,refspec); %set the maximal value to the value at t0
while not(maxvaltemp>ysol(ttemp+1,refspec) && ...
    maxvaltemp>ysol(ttemp-1,refspec))
    ttemp=ttemp+1;
    maxvaltemp=ysol(ttemp,refspec);
end
tmaxind_right=ttemp;

%now search for maximal values of the calcspec between these two indices
[C,I] = max(ysol(tmaxind_left:tmaxind_right,calcspec),[],1);
phase_shift=zeros(length(calcspec),1);
for k=1:length(calcspec)
    phase_shift(k)=tsol((tmaxind_left-1)+I(k))-tsol(tmaxind_left);
end
exact_period=tsol(tmaxind_right)-tsol(tmaxind_left);

end

