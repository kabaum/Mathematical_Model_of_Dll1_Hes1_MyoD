function[dx] = func_hes_dll1_2c3_dde(t,x,z,pars)
% DDE model of two coupled cells of Hes & Dll1 that are identical
%
%variables
%%%%%%%%%%
%x is a vector of length 4 encoding
%x(1): Hes1 in cell 1
%x(2): Dll1 in cell 1 
%x(3): Hes1 in cell 2
%x(4): Dll1 in cell 2
%
%kinetic parameters 
%%%%%%%%%%%%%%%%%%%
%Please note that pars(i) corresponds to k_i in the model description.
%
%pars(1): Hes1 protein synthesis, v in Shimojo 2016: 10
%pars(2): Hes1 regulating itself, K1 in Shimojo 2016: 1
%pars(3): Hes1 degradation, r in Shimojo 2016: 2
%pars(4): Dll1 protein production (inhibited by Hes protein plus time
%lag2)
%pars(5): inhibition constant of Hes1 inhibition of Dll1 protein
%production
%pars(6): Dll1 protein degradation
%pars(7): activation constant of Dll1 activation of Hes of another cell
%
%delays
%%%%%%%
%z(1,1): Hes1 in cell 1 with time lag 1 (tau_1)
%z(1,2): Hes1 in cell 1 with time lag 2 (tau_21)
%z(3,1): Hes1 in cell 2 with time lag 4 (tau_1)
%z(3,2): Hes1 in cell 2 with time lag 5 (tau_21)
%z(4,3): Dll1 in cell 2 with time lag 3 (tau_22)
%z(2,3): Dll1 in cell 1 with time lag 3 (tau_22)
% time lag 1 = tau_1: Hes on itself 
% time lag 2 = tau_21: Hes on Dll1
% time lag 3 = tau_22: Dll1 in one cell on Hes1 in the other cell
%Explicit values of the delays are entered at model integration only.

dx=zeros(4,1); 
 dx(1)=pars(1)*pars(2)^2/(pars(2)^2 + z(1,1)^2)*z(4,3)/pars(7)-pars(3)*x(1); %Hes1 (protein) cell 1
 dx(2)=pars(4)*pars(5)^2/(pars(5)^2 + z(1,2)^2)-pars(6)*x(2); %Dll1 protein cell 1
 dx(3)=pars(1)*pars(2)^2/(pars(2)^2 + z(3,1)^2)*z(2,3)/pars(7)-pars(3)*x(3); %Hes1 (protein) cell 2
 dx(4)=pars(4)*pars(5)^2/(pars(5)^2 + z(3,2)^2)-pars(6)*x(4); %Dll1 protein cell 2
end