# Mathematical model of the genetic network of delta-like 1 in muscle cell development

This repository contains the models and MATLAB code used in 

Zhang Y, Lahmann I, Baum K, Shimojo H, Mourikis P, Wolf J, Kageyama R, Birchmeier C: "Oscillations of Delta-like1 regulate the balance between differentiation and maintenance of muscle stem cells", Nature Communications 2021;12(1):1318
[doi](https://doi.org/10.1038/s41467-021-21631-4)

The models represent qualitative dynamics of the proteins Dll1, Hes1, and MyoD, as well as their mRNA that are relevant players in muscle cell differentiation. Oscillatory dynamics as well as the interplay between two cells were modeled, and ordinary differential equation (ODE) models as well as delay differential equation (DDE) models were used.

Details of the models are described in the Supplementary Methods that can be found in the Supplementary Material file to this paper. You can download the Supplementary Material of Zhang et al. [here](https://static-content.springer.com/esm/art%3A10.1038%2Fs41467-021-21631-4/MediaObjects/41467_2021_21631_MOESM1_ESM.pdf) or access the file stored in this repository [here](./Supplementary_Information_Zhang_et_al_2021.pdf).

Four models have been used in the publication to derive conclusions:

 - The ODE single-cell model [see this folder](./ODE_single-cell_model)
 - The DDE single-cell model [see this folder](./DDE_single-cell_model)
 - The DDE model coupling two identical cells [see this folder](./DDE_model_coupling_two_identical_cells)
 - The DDE model coupling two different cells [see this folder](./DDE_model_coupling_two_different_cells)

The source code of the models are supplied in the above folders. The scripts used for generating the figure panels (Figure 5, Supplementary Figure 6) of the manuscript are also supplied ([see this folder](./script_files)) along with a helper diagnostic function (phase_shift_calculation_gen.m).
To reproduce the figures, download all files from all folders, add them to the MATLAB path and open MATLAB (tested for MATLAB2019b) to run the corresponding scripts.

